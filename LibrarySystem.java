import java.util.ArrayList;
import java.util.List;

class Book {
    private String title;
    private String author;
    private int publishedYear;
    private String isbn;

    public Book(String title, String author, int publishedYear, String isbn) {
        this.title = title;
        this.author = author;
        this.publishedYear = publishedYear;
        this.isbn = isbn;
    }

    public String getBookInfo() {
        return "Title: " + this.title + ", Author: " + this.author + ", Year: " + this.publishedYear + ", ISBN: " + this.isbn;
    }

	public String getAuthor() {
        return this.author;
    }
	
    public int getPublishedYear() {
        return this.publishedYear;
    }

    public String getISBN() {
        return this.isbn;
    }
}

class Library {
    private List<Book> bookList;

    public Library() {
        this.bookList = new ArrayList<>();
    }

    public void addBook(Book book) {
        this.bookList.add(book);
    }

    public void removeBook(String isbn) {
		this.bookList.removeIf(book -> book.getISBN().equals(isbn));
    }

    public List<Book> getBooksPublishedAfterYear(int year) {
        List<Book> result = new ArrayList<>();
        for (Book book : this.bookList) {
            if (book.getPublishedYear() > year) {
                result.add(book);
            }
        }
        return result;
    }

    public List<String> getAuthorsOfBooksPublishedBeforeYear(int year) {
        List<String> result = new ArrayList<>();
        for (Book book : this.bookList) {
            if (book.getPublishedYear() < year) {
                result.add(book.getAuthor());
            }
        }
        return result;
    }
}